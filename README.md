A small project which aims to achieve the objectives set out by the Zynstra technical task.

To initialise the project, a python virtual environment must be created and the required Python packages installed. On a Linux system (assuming sudo privalleges and that you do not already have a python environment package installed), this can be done with the following command line instructions within the project root directory:

```
$ sudo apt-get install python3-venv
$ python3 -m venv venv
$ source venv/bin/activate
$ pip3 install -r requirements.txt
```

At this point, the environment and the project dependencies are ready and the project can be run. There are two scripts that can be run here. The first is the zynstra_test.py script. This script is a pytest testing script that can be run with the following terminal command:

```
$ pytest
```

This will ensure that all of the functionality is still working with the test data.

The main task script (zynstra_task.py) can also be run from this command line. The project accepts a maximum of 2 system args (the file name being one, and the candidate ID being the other). If no candidate ID is provided, then the default value of 13 will be used.

To run the main script, either of the following two examples will work:

```
$ python3 zynstra_task.py
```
or
```
$ python3 zynstra_task.py 7
```

When the terminal requests user input, no file name should be provided. The filename is auto generated as per my understanding of the specification. However, a path to the directory where the file should be stored should be supplied, relative to the current working directory. The answers to the task questions will then be written in JSON format to an answers.json text file within that directory.
