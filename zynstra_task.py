import sys
import requests
import json


class City:
    def __init__(self, name, weather_data):
        self.name = name
        # An array of weather entries that make up the cities weather for the week.
        self.weather_data = weather_data

    def __str__(self):
        return self.name

    # the day input as a string and the time_index as an integer (ex. 3 is equivalent
    # to 3am, 16 is equivalent to 4pm)
    def get_temperarture(self, day, time_index):
        return self.weather_data[day][time_index]["temperature"]

    # the day input as a string to select the day to search for the pressure.
    def get_pressure_falls_below(self, day, limit):
        for entry in self.weather_data[day]:
            if entry["pressure"] < limit:
                return True
        return False

    # Calculate the median temperature for the week in question.
    def get_median_temperature(self):
        temperatures = []
        for day in self.weather_data:
            for entry in self.weather_data[day]:
                temperatures.append(entry["temperature"])
        temperatures.sort()

        # Calculate median based on whether the temperature count is
        # odd or even (expected to be even).
        if len(temperatures) % 2 == 0:
            first = temperatures[int(len(temperatures)/2)]
            second = temperatures[int(len(temperatures)/2) - 1]
            return (first + second) / 2
        else:
            return temperatures[int((len(temperatures) + 1) / 2) - 1]

    # Get the highest wind speed entry for the city
    def get_highest_wind_speed(self):
        wind_speed = 0
        for day in self.weather_data:
            for entry in self.weather_data[day]:
                if entry["wind_speed"] > wind_speed:
                    wind_speed = entry["wind_speed"]

        return wind_speed

    # Provide integer temperature limit and return true if snow is likely
    def is_precipitation_and_below(self, temp_limit):
        for day in self.weather_data:
            for entry in self.weather_data[day]:
                if entry["precipitation"] and entry["temperature"] < temp_limit:
                    return True


# Iterate over all cities and find the city with the highest wind speed entry
def find_highest_wind_speed(city_instances):
    highest_city = ""
    highest_wind_speed = 0
    for city in city_instances:
        wind_speed = city_instances[city].get_highest_wind_speed()
        if wind_speed > highest_wind_speed or (wind_speed == highest_wind_speed and city < highest_city):
            highest_wind_speed = wind_speed
            highest_city = city

    return highest_city


# Iterate over all cities and return true if a city is found that is likely to
# have snow
def is_it_likely_to_snow(city_instances):
    for city in city_instances:
        if city_instances[city].is_precipitation_and_below(2):
            return True

    return False


def main():
    # Assign candidate ID to be used if one isnt provided in system args
    candidate_id = 13
    args_length = len(sys.argv)

    # If candidate ID is provided in system args, then assign it
    if args_length > 1:
        candidate_id = sys.argv[1]

    # Retrieve all cities from the API
    try:
        response = requests.get(
            "http://weather-api.eba-jgjmjs6p.eu-west-2.elasticbeanstalk.com/api/cities/")

        cities = response.json()["cities"]

    except:
        raise

    # Create a dictionary with all of the city name as keys and the city object as the values
    city_instances = {}
    for city in cities:
        try:
            response = requests.get(
                "http://weather-api.eba-jgjmjs6p.eu-west-2.elasticbeanstalk.com/api/weather/{0}/{1}/".format(candidate_id, city))
            if response.status_code == 200:
                data = response.json()

                city_instance = City(city, data)
                city_instances[city] = city_instance

        except:
            raise

    try:
        # Create a dictionary with the question answers
        answers = {
            "What will the temperature be in Bath at 10am on Wednesday morning?": city_instances["bath"].get_temperarture("wednesday", 10),
            "Does the pressure fall below 1000 millibars in Edinburgh at any time on Friday?": city_instances["edinburgh"].get_pressure_falls_below("friday", 1000),
            "What is the median temperature during the week for Cardiff?": city_instances["cardiff"].get_median_temperature(),
            "In which city is the highest wind speed recorded this week?": find_highest_wind_speed(city_instances),
            "Will it snow in any of the cities this week?": is_it_likely_to_snow(city_instances),
        }
        json_answers = json.dumps(answers, indent=4)

        # Ask the user to input a directory path via the terminal
        file_location = str(
            input("Enter the path to the directory (relative to the current working directory) where you wish to save the answers: "))

        # If the user provided directory path does not end in a slash, then add one
        if file_location and file_location[-1] != "/":
            file_location += "/"

        # Create the file that will contain the answers in JSON format, in the user specified directory.
        with open('{0}answers.json'.format(file_location), 'w') as f:
            f.write(json_answers)
    except:
        raise


if __name__ == "__main__":
    main()
