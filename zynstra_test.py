from .zynstra_task import City, find_highest_wind_speed, is_it_likely_to_snow


# Function to define the test data so that it does not need to be manually
# initialised for every test
def define_test_data():
    city_instances = {}
    data_one = {"wednesday": [{"humidity": 71, "precipitation": 6, "pressure": 995, "temperature": 5, "wind_direction": "NE", "wind_speed": 14},
                              {"humidity": 76, "precipitation": 5, "pressure": 996,
                                  "temperature": 8, "wind_direction": "N", "wind_speed": 12},
                              {"humidity": 77, "precipitation": 5, "pressure": 996,
                               "temperature": 6, "wind_direction": "SE", "wind_speed": 17}
                              ], "thursday": [{"humidity": 71, "precipitation": 6, "pressure": 995, "temperature": 4, "wind_direction": "NE", "wind_speed": 14},
                                              {"humidity": 76, "precipitation": 5, "pressure": 996,
                                               "temperature": 8, "wind_direction": "N", "wind_speed": 14},
                                              {"humidity": 77, "precipitation": 5, "pressure": 996,
                                               "temperature": 9, "wind_direction": "SE", "wind_speed": 17}
                                              ]}
    data_two = {"friday": [{"humidity": 71, "precipitation": 6, "pressure": 1000, "temperature": 5, "wind_direction": "NE", "wind_speed": 14},
                           {"humidity": 76, "precipitation": 5, "pressure": 1005,
                               "temperature": 8, "wind_direction": "N", "wind_speed": 14}
                           ]}
    data_three = {"friday": [{"humidity": 71, "precipitation": 6, "pressure": 1008, "temperature": 5, "wind_direction": "NE", "wind_speed": 25},
                             {"humidity": 76, "precipitation": 5, "pressure": 1001,
                                 "temperature": 8, "wind_direction": "N", "wind_speed": 10}
                             ]}
    city_instances["City One"] = City("City One", data_one)
    city_instances["City Two"] = City("City Two", data_two)
    city_instances["City Three"] = City("City Three", data_three)
    return city_instances


# Test for the temperature checker for the specific day and time in a specific city (as
# per question 1)
def test_temp_checker():
    city_instances = define_test_data()

    assert city_instances["City One"].get_temperarture(
        "wednesday", 1) == 8 and city_instances["City Two"].get_temperarture("friday", 0) == 5


# Test for the pressure checker to see if it correctly finds the pressure dropping below
# 1000 in a city on a given day (as per question 2)
def test_pressure_checker():
    city_instances = define_test_data()

    assert (city_instances["City One"].get_pressure_falls_below(
        "thursday", 1000) == True) and (city_instances["City Two"].get_pressure_falls_below("friday", 1000) == False) and (city_instances["City Three"].get_pressure_falls_below("friday", 1000) == False)


# Test for the median temperature finder to see if it can correctly find the median
# temperature for a city for the week (as per question 3)
def test_median_temp_checker():
    city_instances = define_test_data()

    assert city_instances["City One"].get_median_temperature(
    ) == 7.0 and city_instances["City Two"].get_median_temperature() == 6.5


# Test for the highest wind speed finder to see if it can correctly find the
# highest wind speed (as per question 4)
def test_find_highest_wind_speed():
    city_instances = define_test_data()

    assert find_highest_wind_speed(city_instances) == "City Three"


# Repeating the highest wind speed finder test, with an alteration in the data
# which should change the result of the test, for better test coverage
def test_find_highest_wind_speed_2():
    city_instances = define_test_data()

    city_instances["City Two"].weather_data["friday"][0]["wind_speed"] = 100

    assert find_highest_wind_speed(city_instances) == "City Two"


# Test for the snow predictor to see if it can correctly predict whether snow
# is likely in any of the cities (as per question 5)
def test_snow_checker():
    city_instances = define_test_data()

    assert is_it_likely_to_snow(city_instances) == False


# Repeating the snow predictor test, with an alteration in the data which should
# change the result of the test, for better test coverage
def test_snow_checker_2():
    city_instances = define_test_data()

    city_instances["City One"].weather_data["wednesday"][1]["temperature"] = 1

    assert is_it_likely_to_snow(city_instances) == True
